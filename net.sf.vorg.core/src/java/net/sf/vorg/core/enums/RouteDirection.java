package net.sf.vorg.core.enums;

public enum RouteDirection {
	EAST_WEST, SOUTH_NORTH, WEST_EAST, NORTH_SOUTH;
}
