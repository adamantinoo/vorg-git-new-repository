//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.internals;

// - IMPORT SECTION .........................................................................................
import net.sf.vorg.routecalculator.internals.GeoLocation;

// - CLASS IMPLEMENTATION ...................................................................................
public class PilotLocation {
	// - S T A T I C - S E C T I O N ..........................................................................
	// private static Logger logger = Logger.getLogger("net.sf.vorg.vorgautopilot.models");

	// - F I E L D - S E C T I O N ............................................................................
	private GeoLocation	location	= new GeoLocation();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public PilotLocation() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public GeoLocation getLocation() {
		return location;
	}

	public void setLatitude(final String newLatitude) {
		location.setLat(newLatitude);
	}

	public void setLocation(final GeoLocation newLocation) {
		if (null != newLocation) location = new GeoLocation(newLocation.getLat(), newLocation.getLon());
	}

	public void setLongitude(final String newLatitude) {
		location.setLon(newLatitude);
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[PilotLocation ");
		buffer.append(location);
		buffer.append("]");
		return buffer.toString();
	}
}

// - UNUSED CODE ............................................................................................
