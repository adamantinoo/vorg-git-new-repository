//  PROJECT:        net.sf.vorg.core
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.internals;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public enum WaypointTypes {
	NOACTION, VMG, DIRECT, PROJECTED, ANGLE;
	public static WaypointTypes decodeType(String typeName) {
		if (null == typeName) return WaypointTypes.NOACTION;
		if (typeName.toUpperCase().equals("VMG")) return WaypointTypes.VMG;
		if (typeName.toUpperCase().equals("DIRECT")) return WaypointTypes.DIRECT;
		if (typeName.toUpperCase().equals("PROJECTED")) return WaypointTypes.PROJECTED;
		if (typeName.toUpperCase().equals("ANGLE")) return WaypointTypes.ANGLE;
		return NOACTION;
	}
}

// - UNUSED CODE ............................................................................................
