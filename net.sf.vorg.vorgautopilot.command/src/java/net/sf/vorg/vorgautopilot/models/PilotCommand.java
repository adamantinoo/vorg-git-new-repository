//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.models;

//- IMPORT SECTION .........................................................................................
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Vector;
import java.util.logging.Logger;

import org.xml.sax.Attributes;

import net.sf.vorg.core.exceptions.LocationNotInMap;
import net.sf.vorg.routecalculator.internals.ExtendedLocation;
import net.sf.vorg.routecalculator.internals.GeoLocation;
import net.sf.vorg.routecalculator.internals.Polars;
import net.sf.vorg.routecalculator.internals.SailConfiguration;
import net.sf.vorg.routecalculator.models.WindCell;
import net.sf.vorg.routecalculator.models.WindMapHandler;
import net.sf.vorg.vorgautopilot.internals.CommandStatus;
import net.sf.vorg.vorgautopilot.internals.CommandTypes;
import net.sf.vorg.vorgautopilot.internals.WaypointTypes;

//- CLASS IMPLEMENTATION ...................................................................................
/**
 * This class processes the pilot command section of the boat configuration file and generates the set of
 * commands and waypoints that describe the new boat instructions.
 */
public class PilotCommand {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger				logger				= Logger.getLogger("net.sf.vorg.vorgautopilot.models");

	// - F I E L D - S E C T I O N ............................................................................
	private final Boat					boatRef;
	private CommandTypes				type					= CommandTypes.NOCOMMAND;
	private Vector<Waypoint>		waypointList	= new Vector<Waypoint>();
	private Vector<PilotLimits>	limits				= new Vector<PilotLimits>();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public PilotCommand(final String typeName, final Boat parentBoat) {
		type = CommandTypes.decodeType(typeName);
		boatRef = parentBoat;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public CommandTypes getType() {
		return type;
	}

	public void pilot() {
		System.out.println(Calendar.getInstance().getTime() + " - " + "Processing configuration command" + this);
		if (type == CommandTypes.NOCOMMAND) return;
		if (type == CommandTypes.ROUTE) pilotFollowRoute(boatRef);
	}

	public void setWaypointList(final Vector<Waypoint> buildWaypointList) {
		waypointList = buildWaypointList;
	}

	public void startElement(final String name, final Attributes attributes) {
		if (name.toLowerCase().equals("pilotlimits")) limits = new Vector<PilotLimits>();
		if (name.toLowerCase().equals("limit")) {
			final PilotLimits newLimit = new PilotLimits();
			newLimit.setBorder(attributes.getValue("border"));
			newLimit.setLatitude(attributes.getValue("latitude"));
			newLimit.setLongitude(attributes.getValue("longitude"));
			newLimit.setAction(attributes.getValue("action"));
			limits.add(newLimit);
		}
		if (name.toLowerCase().equals("waypointlist")) waypointList = new Vector<Waypoint>();
		if (name.toLowerCase().equals("waypoint")) {
			final WaypointTypes type = WaypointTypes.decodeType(attributes.getValue("type"));
			if (type == WaypointTypes.PROJECTED) // - If this waypoint if of type PROJECTED then it needs another
				// waypoint for reference.
				try {
					final ProjectedWaypoint waypoint = new ProjectedWaypoint(attributes.getValue("type"), waypointList
							.lastElement());
					waypoint.setName(attributes.getValue("name"));
					waypoint.setLatitude(attributes.getValue("latitude"));
					waypoint.setLongitude(attributes.getValue("longitude"));
					waypoint.setRange(attributes.getValue("range"));
					waypoint.setMinAWD(attributes.getValue("minAWD"));
					waypoint.setMaxAWD(attributes.getValue("maxAWD"));
					waypointList.add(waypoint);
				} catch (final NoSuchElementException nsee) {
					System.out.println("WWW - PROJECTED waypoints cannot be the first point in the route. Skipped ");
				}
			if (type == WaypointTypes.ANGLE)
				// - If this waypoint if of type PROJECTED then it needs another waypoint for reference.
				try {
					final int angle = new Integer(attributes.getValue("angle")).intValue();
					final ProjectedWaypoint waypoint = new ProjectedWaypoint(attributes.getValue("type"), angle);
					waypoint.setName(attributes.getValue("name"));
					waypoint.setLatitude(attributes.getValue("latitude"));
					waypoint.setLongitude(attributes.getValue("longitude"));
					waypoint.setRange(attributes.getValue("range"));
					waypoint.setMinAWD(attributes.getValue("minAWD"));
					waypoint.setMaxAWD(attributes.getValue("maxAWD"));
					waypointList.add(waypoint);
				} catch (final NoSuchElementException nsee) {
					System.out.println("WWW - PROJECTED waypoints cannot be the first point in the route. Skipped ");
				}
			else {
				final Waypoint waypoint = new Waypoint(attributes.getValue("type"));
				waypoint.setName(attributes.getValue("name"));
				waypoint.setLatitude(attributes.getValue("latitude"));
				waypoint.setLongitude(attributes.getValue("longitude"));
				waypoint.setRange(attributes.getValue("range"));
				waypoint.setMinAWD(attributes.getValue("minAWD"));
				// waypoint.setMaxAWD(attributes.getValue("maxAWD"));
				waypointList.add(waypoint);
			}
		}
	}

	public CommandStatus testActivity() {
		// - Check if the command is active or inactive by limits.
		CommandStatus status = CommandStatus.NOCHANGE;
		final Iterator<PilotLimits> lit = limits.iterator();
		while (lit.hasNext()) {
			final PilotLimits limit = lit.next();
			final CommandStatus test = limit.testLimit(boatRef.getLocation());
			if (test != CommandStatus.NOCHANGE) status = test;
		}
		return status;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("\n[PilotCommand ");
		buffer.append("type=").append(type).append("\n");
		// if (type == CommandTypes.VMG) buffer.append("waypoint=").append(waypoint).append("]");
		/* if (type == CommandTypes.TRACK) */
		// buffer.append("\n").append("waypoints=").append(waypointList).append("]");
		// buffer.append("\n              ").append("Waypoint List ").append("[\n");
		final Iterator<Waypoint> wit = waypointList.iterator();
		while (wit.hasNext()) {
			final Waypoint wp = wit.next();
			buffer.append("").append(wp).append("\n");
		}
		// buffer.append(" ");
		// buffer.append("\n").append("limits=").append(limits).append("]");
		// buffer.append("]");
		return buffer.toString();
	}

	private double adjustSailAngle(final GeoLocation boatLocation, final Waypoint waypoint) throws LocationNotInMap {
		final WindCell windCell = WindMapHandler.getWindCell(boatLocation);
		double angle = boatLocation.angleTo(waypoint.getLocation());
		double apparentHeading = GeoLocation.calculateAWD(windCell.getWindDir(), angle);
		SailConfiguration sails = Polars.lookup(new Double(apparentHeading).intValue(), windCell.getWindSpeed());

		// - If angle is less that the AWD adjust it. Also adjust id speed < 0.0
		boolean search = (sails.getSpeed() == 0.0) | (Math.abs(Math.round(apparentHeading)) < Math.abs(waypoint.getAWD()));
		int displacement = 1;
		if (apparentHeading > 0)
			displacement = -1;
		else
			displacement = 1;
		while (search) {
			angle += displacement;
			apparentHeading = GeoLocation.calculateAWD(windCell.getWindDir(), angle);
			sails = Polars.lookup(new Double(apparentHeading).intValue(), windCell.getWindSpeed());
			search = (sails.getSpeed() == 0.0) | (Math.abs(Math.round(apparentHeading)) < Math.abs(waypoint.getAWD()));
			// if (displacement < 0)
			// displacement--;
			// else
			// displacement++;
		}
		return angle;
	}

	private void pilotFollowRoute(final Boat boat) {
		// - Get the current boat location.
		final ExtendedLocation boatLocation = new ExtendedLocation(boat.getLocation());
		final Iterator<Waypoint> wit = waypointList.iterator();
		while (wit.hasNext()) {
			final Waypoint wp = wit.next();
			// - Check if the waypoint is of NOCOMMAND type.
			if (!wp.isActive()) continue;
			// - Test if this waypoint has been surpassed.
			if (boatRef.isSurpassed(wp)) continue;

			// - Make the waypoint to check its own surpassed algorithm.
			if (wp.isSurpassed(boatLocation)) {
				boatRef.surpassWaypoint(wp);
				continue;
			}

			try {
				// - Depending on waypoint type use the code to generate the command.
				final NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
				nf.setMaximumFractionDigits(3);
				System.out.println("\nActivating route to waypoint: \n" + wp.activationReport(boatLocation));
				wp.sendCommand(boat);
				return;
			} catch (final LocationNotInMap lnime) {
				System.out.println("EEE " + lnime.getMessage());
			}
		} /* END while */
	}

	public void addLimit(PilotLimits newLimit) {
		if (null != newLimit) limits.add(newLimit);
	}

	// private void pilotFollowVMG(final Boat boat) {
	// // // - Check if the command is active or inactive by limits.
	// // CommandStatus active = CommandStatus.NOCHANGE;
	// // Iterator<PilotLimits> lit = limits.iterator();
	// // while (lit.hasNext()) {
	// // PilotLimits limit = lit.next();
	// // CommandStatus test = limit.testLimit(boat.getLocation());
	// // if (test != CommandStatus.NOCHANGE) active = test;
	// // System.out.println("Testing limit " + limit + " with the result of: " + active);
	// // }
	// // if (active == CommandStatus.GO) {
	// // - Get the current boat location
	// final GeoLocation boatLocation = boat.getLocation();
	// final Iterator<Waypoint> wit = waypointList.iterator();
	// while (wit.hasNext()) {
	// final Waypoint wp = wit.next();
	// if (boatRef.isSurpassed(wp)) continue;
	//
	// // - Calculate distance to waypoint. If less than 80 miles discard it.
	// final GeoLocation waypointLocation = wp.getLocation();
	// final double distance = waypointLocation.distance(boatLocation);
	// if (distance < wp.getRange()) {
	// System.out.println("Waypoint " + waypointLocation + " discarded. Distance " + distance +
	// " below 60 miles.");
	// boatRef.surpassWaypoint(wp);
	// continue;
	// } else {
	// System.out.println("\nActivating route to waypoint: " + wp);
	// pilotMaxVMG(boat, waypointLocation);
	// return;
	// }
	// }
	// }

	// private void pilotMaxVMG(final Boat boat, final GeoLocation destination) {
	// // CommandStatus active = CommandStatus.NOCHANGE;
	// // Iterator<PilotLimits> lit = limits.iterator();
	// // while (lit.hasNext()) {
	// // PilotLimits limit = lit.next();
	// // CommandStatus test = limit.testLimit(boat.getLocation());
	// // if (test != CommandStatus.NOCHANGE) active = test;
	// // System.out.println("Testing limit " + limit + " with the result of: " + active);
	// // }
	// // if (active == CommandStatus.GO) {
	// // - Get the wind cell of the current boat location.
	// final VMCRouter route = new VMCRouter(destination);
	// try {
	// final VMCData vmc = route.getBestVMG(boat.getLocation());
	// // - Check if current boat configuration is the same as requested.
	// // System.out.println("VMG");
	// System.out.println();
	// System.out.println(vmc.printReport());
	// System.out.println();
	// // System.out.println(vmc.printRecord());
	// final BoatCommand newBoatCommand = new BoatCommand();
	// newBoatCommand.setHeading(boat.getHeading(), vmc.getBestAngle());
	// newBoatCommand.setSails(vmc.getBestSailConfiguration());
	// newBoatCommand.sendCommand(boat);
	// } catch (final LocationNotInMap e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// // }
	// }
}
// - UNUSED CODE ............................................................................................
