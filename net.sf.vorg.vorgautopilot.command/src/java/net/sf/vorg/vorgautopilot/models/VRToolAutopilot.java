//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.models;

// - IMPORT SECTION .........................................................................................
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Calendar;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

import main.VORGAutopilot;
import net.sf.vorg.vorgautopilot.parsers.PointsParserHandler;
import net.sf.vorg.vorgautopilot.parsers.VRToolNavParser;

// - CLASS IMPLEMENTATION ...................................................................................
public class VRToolAutopilot extends XMLAutopilot {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vorg.vorgautopilot.models");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public VRToolAutopilot(final String configurationFileName) {
		configFileName = configurationFileName;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public void run() {
		try {
			// super.run();
			// - Before starting, read the list of persistent points already surpassed.
			readSurpassed(boat);

			// - Start the endless loop inside the thread.
			if (VORGAutopilot.getRefresh() != 10) {
				long lastMinute = minuteOfDay() - 100;
				while (!XMLAutopilot.terminationRequested)
					try {
						checkMapReload();
						// - Check if we have entered a new 10 minute interval
						final long dayMinute = minuteOfDay();
						if ((dayMinute < lastMinute) || (dayMinute >= lastMinute + VORGAutopilot.getRefresh())) {
							lastMinute = dayMinute;
							performOperation();
						}
						// - Sleep the process for a minute before checking again the waiting interval.
						try {
							Thread.sleep(XMLAutopilot.SLEEP_MSECONDS);
						} catch (final InterruptedException ie) {
							System.out.println("Autopilot interrupted. Terminating current process.");
						}
					} catch (final Exception ex) {
						// - Any class of exception. Record it and continue.
						System.out.println("EEE EXCEPTION - " + ex.getMessage());
					}
			} else {
				int lastMinute = -1; // Reset the interval detection to run for the first time
				while (!XMLAutopilot.terminationRequested)
					try {
						checkMapReload();
						// - Check if we have entered a new 10 minute interval
						final Calendar now = Calendar.getInstance();
						final int minute = now.get(Calendar.MINUTE) / 10;
						if (minute != lastMinute) {
							// - Wait until minute 1 and 5 seconds.
							int minutes = now.get(Calendar.MINUTE);
							final int seconds = now.get(Calendar.SECOND);
							// - If the minute is multiple of 10, wait.
							minutes = minutes % 10;
							if (minutes == 0) try {
								if (VORGAutopilot.timeDelay > 0) Thread.sleep((VORGAutopilot.timeDelay - seconds) * 1000);
							} catch (final InterruptedException ie) {
								System.out.println("Autopilot interrupted. Terminating current process.");
							} catch (final IllegalArgumentException iae) {
								// - The value to wait is not valid. Skip
							}
							lastMinute = minute;
							performOperation();
						} else
							// - Sleep the process for a minute before checking again the ten minute period
							try {
								Thread.sleep(XMLAutopilot.SLEEP_MSECONDS);
							} catch (final InterruptedException ie) {
								System.out.println("Autopilot interrupted. Terminating current process.");
							}
					} catch (final Exception ex) {
						// - Any class of exception. Record it and continue.
						System.out.println("EEE EXCEPTION - " + ex.getMessage());
					}
			}
			System.out.println("Termination request acknowledged.");
		} catch (final Exception ex) {
			// - Any class of exception. Record it and continue.
			System.out.println("EEE EXCEPTION - " + ex.getMessage());
		}
	}

	private void readSurpassed(final Boat boat) {
		// - Load boat information. The boat now is empty.
		processVRToolConfiguration();
		// - Open the file with the boat name.
		if (null != boat.getName())
			try {
				final BufferedInputStream pointListFile = new BufferedInputStream(new FileInputStream(boat.getName()
						+ ".points"));
				final SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
				parser.parse(pointListFile, new PointsParserHandler(boat));
			} catch (final FileNotFoundException fnfe) {
				// TODO Auto-generated catch block
				fnfe.printStackTrace();
			} catch (final ParserConfigurationException pce) {
				// TODO Auto-generated catch block
				pce.printStackTrace();
			} catch (final SAXException saxe) {
				// TODO Auto-generated catch block
				saxe.printStackTrace();
			} catch (final IOException ioe) {
				// TODO Auto-generated catch block
				ioe.printStackTrace();
			}
	}

	private void performOperation() {
		// - Process current configuration. Read the configuration file to load any updates.
		processVRToolConfiguration();
		// - Get current boat data
		getBoatData(cookies);
		// - Execute any of the boat commands
		boat.performPilot();
	}

	/*
	 * Reads and parses the configuration XML file to get the identification information and the autopilot
	 * commands.
	 */
	private boolean processVRToolConfiguration() {
		final VRToolNavParser parser = new VRToolNavParser(boat);
		parser.setInput(configFileName);
		boat.clearCommands();
		System.out.println("------------------------------------------------------------------------------------------");
		System.out.println(Calendar.getInstance().getTime() + " - new scan run for configuration: " + configFileName);
		parser.parse(boat);
		if (null == boat.getName()) {
			System.out.println("Not detected any pilot configuration data on file:" + configFileName);
			return false;
		} else
			return true;
	}

}

// - UNUSED CODE ............................................................................................
