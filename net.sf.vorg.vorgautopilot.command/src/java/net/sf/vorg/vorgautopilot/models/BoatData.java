//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen 
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) 
//  RELEASE:        $Revision: 174 
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.models;

// - IMPORT SECTION .........................................................................................
import java.text.NumberFormat;
import java.util.Locale;

import net.sf.vorg.core.enums.Sails;
import net.sf.vorg.core.exceptions.LocationNotInMap;
import net.sf.vorg.routecalculator.internals.GeoLocation;
import net.sf.vorg.routecalculator.internals.VMCData;
import net.sf.vorg.routecalculator.models.WindCell;
import net.sf.vorg.routecalculator.models.WindMapHandler;
import net.sf.vorg.vorgautopilot.internals.PilotLocation;

// - CLASS IMPLEMENTATION ...................................................................................
public class BoatData {
	// - S T A T I C - S E C T I O N ..........................................................................
	// private static Logger logger = Logger.getLogger("net.sf.vorg.vorgautopilot.models");

	// - F I E L D - S E C T I O N ............................................................................
	// - BOAT data.
	protected String						name			= null;
	private final PilotLocation	location	= new PilotLocation();
	private Sails								sail			= Sails.JIB;
	private double							speed			= 0.0;
	private double							windSpeed	= 0.0;
	private int									windAngle	= 0;
	private int									heading		= 0;
	private int									ranking;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	// - M E T H O D - S E C T I O N ..........................................................................
	public int getHeading() {
		return heading;
	}

	public GeoLocation getLocation() {
		return location.getLocation();
	}

	public Sails getSail() {
		return sail;
	}

	public double getSpeed() {
		return speed;
	}

	public int getWindAngle() {
		return windAngle;
	}

	public String getName() {
		return name;
	}

	public double getWindSpeed() {
		return windSpeed;
	}

	public String printReport() throws LocationNotInMap {
		final NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
		nf.setMaximumFractionDigits(3);
		nf.setMinimumFractionDigits(3);
		final StringBuffer buffer = new StringBuffer(toString());
		// - Add current wind cell information.
		WindMapHandler.loadWinds(getLocation());
		final WindCell cell = WindMapHandler.getWindCell(getLocation());
		buffer.append("[Wind Cell Data ").append("direction=").append(cell.getWindDir()).append(", ");
		buffer.append("speed=").append(nf.format(cell.getWindSpeed())).append(", ");
		buffer.append("\n                location=").append(cell.getLocation()).append("]\n");

		// - Add current information for course VMG
		final VMCData vmg = new VMCData(heading, cell);
		buffer.append("\n").append(vmg.printReport());
		return buffer.toString();
	}

	public void setHeading(final int heading) {
		this.heading = heading;
	}

	public void setLatitude(final String latitude) {
		location.setLatitude(latitude);
	}

	public void setLongitude(final String longitude) {
		location.setLongitude(longitude);
	}

	public void setName(final String name) {
		this.name = name;
	}

	public void setRanking(final int classification) {
		ranking = classification;
	}

	public void setSail(final Sails sail) {
		this.sail = sail;
	}

	public void setSail(final String sailCode) {
		sail = Sails.decodeSail(sailCode);
	}

	public void setSpeed(final double speed) {
		this.speed = speed;
	}

	public void setWindAngle(final int windAngle) {
		this.windAngle = windAngle;
	}

	public void setWindSpeed(final double windSpeed) {
		this.windSpeed = windSpeed;
	}

	@Override
	public String toString() {
		final NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
		nf.setMaximumFractionDigits(3);
		nf.setMinimumFractionDigits(3);
		final StringBuffer buffer = new StringBuffer();
		buffer.append("ranking=").append(ranking).append("");
		buffer.append("\n      location=").append(location.getLocation()).append("");
		buffer.append("\n      heading=").append(heading).append(", ");
		buffer.append("AWD=").append(windAngle).append(", ");
		buffer.append("speed=").append(nf.format(speed)).append(" knots, ");
		buffer.append("sail=").append(sail).append("");
		buffer.append("\n      windSpeed=").append(nf.format(windSpeed)).append(" knots");
		buffer.append("]\n");
		return buffer.toString();
	}
}

// - UNUSED CODE ............................................................................................
