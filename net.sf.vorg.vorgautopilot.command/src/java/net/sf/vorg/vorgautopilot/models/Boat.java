//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen 
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) 
//  RELEASE:        $Revision: 174 
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.models;

// - IMPORT SECTION .........................................................................................
import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringBufferInputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.NumberFormat;
import java.util.Iterator;
import java.util.Locale;
import java.util.Vector;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

import net.sf.vorg.core.exceptions.DataLoadingException;
import net.sf.vorg.vorgautopilot.internals.CommandStatus;
import net.sf.vorg.vorgautopilot.internals.VORGURLRequest;
import net.sf.vorg.vorgautopilot.internals.WaypointTypes;
import net.sf.vorg.vorgautopilot.parsers.BoatDataParserHandler;

// - CLASS IMPLEMENTATION ...................................................................................
public class Boat extends BoatData {
	// - S T A T I C - S E C T I O N ..........................................................................
	// private static Logger logger = Logger.getLogger("net.sf.vorg.vorgautopilot.models");

	// - F I E L D - S E C T I O N ............................................................................
	// - USER data.
	private String									email;
	private String									userid;
	private String									name;
	private String									password;
	private String									clef;

	// - COMMAND data.
	private Vector<PilotCommand>		commands						= new Vector<PilotCommand>();
	private final Vector<Waypoint>	surpassedWaypoints	= new Vector<Waypoint>();
	private final Vector<Boat>			friends							= new Vector<Boat>();
	private Exception								lastException				= null;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Boat() {
	}

	// public Boat(final String value) {
	// userid = value;
	// }

	// - M E T H O D - S E C T I O N ..........................................................................
	public void addCommand(final PilotCommand command) {
		commands.add(command);
	}

	public void addFriend(final Boat newFriend) {
		friends.add(newFriend);
	}

	public void clearCommands() {
		commands = new Vector<PilotCommand>();
	}

	public String getClef() {
		return clef;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

	public String getUserid() {
		return userid;
	}

	/**
	 * Return true or false depending if the waypoint is or not inside the list of points already walked. The
	 * test is made based on the distance between the target test waypoint and the waypoint in the list. If the
	 * distance is less than one mile then we consider that we have reached that point and a match is found.
	 */
	public boolean isSurpassed(final Waypoint waypoint) {
		final Iterator<Waypoint> wit = surpassedWaypoints.iterator();
		while (wit.hasNext()) {
			final Waypoint testWaypoint = wit.next();
			final double distance = testWaypoint.getLocation().distance(waypoint.getLocation());
			if (distance < 1.0) return true;
		}
		return false;
	}

	/*
	 * Order the list of commands and process them in order. Also use a criteria to detect command
	 * incompatibilities.
	 */
	public void performPilot() {
		// - Scan the commands to test which of them are active.
		final Vector<PilotCommand> activeCommands = new Vector<PilotCommand>();
		final Iterator<PilotCommand> cit = commands.iterator();
		while (cit.hasNext()) {
			final PilotCommand command = cit.next();
			final CommandStatus status = command.testActivity();
			if (status == CommandStatus.GO) activeCommands.add(command);
		}

		// - Process only the first commend if some.
		if (activeCommands.size() > 0) {
			final PilotCommand command = activeCommands.firstElement();
			if (null != command)
				command.pilot();
			else
				System.out.println("No commands active. Waiting for activation.");
		}
	}

	public void setClef(final String key) {
		clef = key;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public void setPassword(final String password) {
		this.password = password;
	}

	public void setUserid(final String userid) {
		this.userid = userid;
	}

	/**
	 * Add a new point to the list of walked points. This call also updates the persistent file that keeps track
	 * of this list of points.
	 */
	public void surpassWaypoint(final Waypoint waypoint) {
		surpassWaypointDirect(waypoint);
		// - Update the contents of the persistent file after this data is added.
		dumpSurpassedWaypoints();
	}

	/**
	 * Just add this point to the list of walked points. During the addition process, check for duplicates and
	 * do not add a point already on the list or close to the point.
	 */
	public void surpassWaypointDirect(final Waypoint waypoint) {
		// - Check for duplicated before adding this point.
		final Iterator<Waypoint> wit = surpassedWaypoints.iterator();
		boolean found = false;
		while (wit.hasNext()) {
			final Waypoint testWp = wit.next();
			if (testWp.isEquivalent(waypoint)) {
				found = true;
				break;
			}
		}
		if (!found) surpassedWaypoints.add(waypoint);
	}

	public void getBoatData() throws DataLoadingException {
		// - First check if there is a name configured for this boat.
		if (null != getName()) {
			final StringBuffer request = new StringBuffer("/get_user.php?");
			try {
				request.append("pseudo=" + URLEncoder.encode(getName(), "UTF-8"));
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// request.append("&").append("id_user_searching=").append(boat.getUserid());

			final VORGURLRequest boatRequest = new VORGURLRequest(request.toString());
			// cit = cookies.iterator();
			// final StringBuffer cookies2 = new StringBuffer();
			// cookies2.append("userid=" + boat.getUserid());
			// cookies2.append("useremail=" + boat.getEmail());
			boatRequest.executeGET(null);
			// FIXME This lines have to be removed to save memory.
			final String data = boatRequest.getData();
			parseBoatData(data);
			if (null == lastException) {
				// - Add current boat location point to the list of surpassed points.
				final Waypoint waypoint = new Waypoint(WaypointTypes.DIRECT.toString());
				waypoint.setLocation(getLocation());
				surpassWaypoint(waypoint);
			} else
				// - Some exception has been detected. Invalidate this boat structure.
				throw new DataLoadingException("Some Exception on the loading of boat data. " + lastException.getMessage());

			// System.out.println();
			// try {
			// System.out.println(this.printReport());
			// System.out.println();
			// } catch (final LocationNotInMap e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
		}
	}

	private void parseBoatData(final String data) {
		// - Parse the configuration file.
		InputStream stream;
		try {
			lastException = null;
			stream = new BufferedInputStream(new StringBufferInputStream(data));
			final SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
			final BoatDataParserHandler handler = new BoatDataParserHandler(this);
			parser.parse(stream, handler);
		} catch (final FileNotFoundException fnfe) {
			lastException = fnfe;
			fnfe.printStackTrace();
		} catch (final ParserConfigurationException pce) {
			lastException = pce;
			pce.printStackTrace();
		} catch (final SAXException saxe) {
			lastException = saxe;
			saxe.printStackTrace();
		} catch (final IOException ioe) {
			lastException = ioe;
			ioe.printStackTrace();
		}
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[Boat ");
		buffer.append("name=").append(getName()).append(", ");
		buffer.append(super.toString());
		return buffer.toString();
	}

	private void dumpSurpassedWaypoints() {
		try {
			final PrintWriter pointListFile = new PrintWriter(getName() + ".points");
			final NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
			nf.setMaximumFractionDigits(6);
			nf.setMinimumFractionDigits(6);
			StringBuffer buffer = new StringBuffer();
			buffer.append("<?xml version=").append(quote("1.0")).append(" encoding=").append(quote("UTF-8")).append("?>")
					.append("\n");
			buffer.append("<waypointlist>");
			pointListFile.println(buffer.toString());
			final Iterator<Waypoint> wit = surpassedWaypoints.iterator();
			while (wit.hasNext()) {
				final Waypoint way = wit.next();
				buffer = new StringBuffer();
				buffer.append("  <waypoint type=").append(quote("DIRECT")).append(" ");
				buffer.append("latitude=").append(quote(nf.format(way.getLocation().getLat()))).append(" ");
				buffer.append("longitude=").append(quote(nf.format(way.getLocation().getLon()))).append(" ");
				buffer.append(" />");
				pointListFile.println(buffer.toString());
			}
			pointListFile.println("</waypointlist>");
			pointListFile.flush();
			pointListFile.close();
		} catch (final FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private String quote(final String value) {
		return '"' + value + '"';
	}
}

// - UNUSED CODE ............................................................................................
