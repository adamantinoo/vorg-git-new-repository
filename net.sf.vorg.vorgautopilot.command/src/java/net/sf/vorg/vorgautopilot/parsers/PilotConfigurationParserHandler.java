//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.parsers;

// - IMPORT SECTION .........................................................................................
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import net.sf.vorg.vorgautopilot.models.Boat;
import net.sf.vorg.vorgautopilot.models.PilotCommand;

// - CLASS IMPLEMENTATION ...................................................................................
public class PilotConfigurationParserHandler extends DefaultHandler {
	// - S T A T I C - S E C T I O N ..........................................................................
	// private static Logger logger = Logger.getLogger("net.sf.vorg.vorgautopilot.models");

	// - F I E L D - S E C T I O N ............................................................................
	private final Boat		boatRef;
	private PilotCommand	buildUpCommand;

	// private final Vector<Waypoint> buildWaypointList = new Vector<Waypoint>();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public PilotConfigurationParserHandler(final Boat boat) {
		boatRef = boat;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/** Parse the configuration file and store the parameters into the single boat instance. */
	@Override
	public void startElement(final String uri, final String localName, final String name, final Attributes attributes)
			throws SAXException {
		// - Store the information for the user account for login purposes
		if (name.toLowerCase().equals("user")) {
			boatRef.setEmail(attributes.getValue("useremail"));
			boatRef.setUserid(attributes.getValue("userid"));
			boatRef.setPassword(attributes.getValue("password"));
			boatRef.setClef(attributes.getValue("clef"));
		}
		// - Get the boat name to be managed.
		if (name.toLowerCase().equals("boat")) boatRef.setName(attributes.getValue("name"));

		// - Read and create the list of commands to be processed for the boat.
		if (name.toLowerCase().equals("pilotcommand")) {
			buildUpCommand = new PilotCommand(attributes.getValue("type"), boatRef);
			buildUpCommand.startElement(name, attributes);
		}
		if (name.toLowerCase().equals("pilotlimits")) buildUpCommand.startElement(name, attributes);
		if (name.toLowerCase().equals("limit")) buildUpCommand.startElement(name, attributes);
		if (name.toLowerCase().equals("waypointlist")) buildUpCommand.startElement(name, attributes);
		if (name.toLowerCase().equals("waypoint")) buildUpCommand.startElement(name, attributes);
	}

	@Override
	public void endElement(String uri, String localName, String name) throws SAXException {
		super.endElement(uri, localName, name);
		if (name.toLowerCase().equals("pilotcommand")) boatRef.addCommand(buildUpCommand);
	}
}

// - UNUSED CODE ............................................................................................
