//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.models;

// - IMPORT SECTION .........................................................................................
import junit.framework.TestCase;
import net.sf.vorg.routecalculator.internals.GeoLocation;
import net.sf.vorg.routecalculator.internals.Polars;

//- CLASS IMPLEMENTATION ...................................................................................
public class WindCellTest extends TestCase {
	private final Map			testMap;
	private final Polars	testPolars;

	// - S T A T I C - S E C T I O N ..........................................................................
	// private static Logger logger = Logger.getLogger("net.sf.vgap4.assistant.test");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public WindCellTest() {
		testMap = new Map();
		testMap.setBoatPosition(new GeoLocation(2, 04, 102, 07));
		testMap.setDestination(new GeoLocation(1, 32, 102, 56));
		testPolars = new Polars();
		testPolars.addWindData(318, 10.8, 122, 10.8);
		testPolars.addWindData(318, 10.8, 128, 11.0);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void testGeoToDegrees() throws Exception {
		assertEquals(100.5, new GeoUnit(100, 30).toDegrees());
	}

	public void testAngleToLocation() throws Exception {
		GeoLocation start = new GeoLocation(new GeoUnit(2, 04).toDegrees(), new GeoUnit(102, 07).toDegrees());
		GeoLocation end = new GeoLocation(new GeoUnit(1, 32).toDegrees(), new GeoUnit(102, 56).toDegrees());
		assertEquals(123.1470, start.angleTo(end), 0.0001);

		end = new GeoLocation(2, 30, 101, 31);
		assertEquals(54.1623, start.angleTo(end), 0.0001);
	}

	public void testMapInitialization() throws Exception {
		assertEquals(new GeoUnit(2, 04).toDegrees(), testMap.getBoatLat(), 0.0001);
		assertEquals(new GeoUnit(102, 07).toDegrees(), testMap.getBoatLon(), 0.0001);
		assertEquals(new GeoUnit(1, 32).toDegrees(), testMap.getDestinationLat(), 0.0001);
		assertEquals(new GeoUnit(102, 56).toDegrees(), testMap.getDestinationLon(), 0.0001);
	}

	public void testPolarLoadup() throws Exception {
	}

	public void testPolarLookup() throws Exception {

	}
}

class GeoUnit {
	private final int	grade;
	private final int	minute;

	public GeoUnit(int grade, int minute) {
		this.grade = grade;
		this.minute = minute;
	}

	public static double toDegrees(int grade, int minute) {
		return grade * 1.0 + minute / 60.0;
	}

	public double toDegrees() {
		return grade * 1.0 + minute / 60.0;
	}
}
// - UNUSED CODE ............................................................................................
