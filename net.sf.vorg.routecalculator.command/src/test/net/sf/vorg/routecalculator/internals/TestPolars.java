//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen 
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) 
//  RELEASE:        $Revision: 174 
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.internals;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import junit.framework.TestCase;

// - CLASS IMPLEMENTATION ...................................................................................
public class TestPolars extends TestCase {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vorg.routecalculator.internals");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public TestPolars() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void testLookup() {
		// assertEquals(2.7, Polars.lookup(158, 3.8));
		// assertEquals(4.7, Polars.lookup(130, 3.8));
		// assertEquals(2.7, Polars.lookup(158, 3.8));
		// assertEquals(2.7, Polars.lookup(158, 3.8));
		// assertEquals(2.7, Polars.lookup(158, 3.8));
		// assertEquals(2.7, Polars.lookup(158, 3.8));
		// assertEquals(2.7, Polars.lookup(158, 3.8));
		// assertEquals(2.7, Polars.lookup(158, 3.8));
		// assertEquals("Calculating polar for below 30 angles: 16 degrees", 1.0, Polars.lookup(16, 6.5));

		// assertEquals(2.7, Polars.lookup(79, 13.8));
		SailConfiguration sail = Polars.lookup(79, 13.8);
		assertEquals(12.9, Polars.lookup(79, 13.8));
	}

	public void testCalcApparent() {
		fail("Not yet implemented");
	}
}

// - UNUSED CODE ............................................................................................
