//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.internals;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public class Boundaries {
	// - S T A T I C - S E C T I O N ..........................................................................
	// private static Logger logger = Logger.getLogger("net.sf.vorg.routecalculator.models");

	// - F I E L D - S E C T I O N ............................................................................
	private double	north	= 0.0;
	private double	south	= 0.0;
	private double	west	= 0.0;
	private double	east	= 0.0;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Boundaries() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public double getEast() {
		return east;
	}

	public double getNorth() {
		return north;
	}

	public double getSouth() {
		return south;
	}

	public double getWest() {
		return west;
	}

	public void setEast(final double location) {
		east = location;
	}

	public void setNorth(final double location) {
		north = location;
	}

	public void setSouth(final double location) {
		south = location;
	}

	public void setWest(final double location) {
		west = location;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[Boundaries ");
		buffer.append("north=").append(north).append(",");
		buffer.append("east=").append(east).append(",");
		buffer.append("south=").append(south).append(",");
		buffer.append("west=").append(west).append("]");
		return buffer.toString();
	}
}

// - UNUSED CODE ............................................................................................
